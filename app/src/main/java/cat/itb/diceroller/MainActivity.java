package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageView dado1;
    private ImageView dado2;
    private Button button;
    private Button restartButton;
    int [] caras = {R.drawable.dice_1, R.drawable.dice_2,
                    R.drawable.dice_3, R.drawable.dice_4,
                    R.drawable.dice_5, R.drawable.dice_6};
    private Toast toast;
    private int n1;
    private int n2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dado1 = findViewById(R.id.dado1_img);
        dado2 = findViewById(R.id.dado2_img);
        button = findViewById(R.id.roll_button);
        restartButton = findViewById(R.id.restart_button);

        restartButton.setOnClickListener(this);
        button.setOnClickListener(this);
        dado1.setOnClickListener(this);
        dado2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.roll_button:
                n1 = randomNumero();
                n2 = randomNumero();
                setCara(dado1, dado2, n1, n2);
                break;

            case R.id.restart_button:
                dado1.setVisibility(View.INVISIBLE);
                dado2.setVisibility(View.INVISIBLE);
                Toast.makeText(this, "RESTART", Toast.LENGTH_SHORT).show();
                break;

            case R.id.dado1_img:
                n1 = randomNumero();
                dado1.setImageResource(caras[n1]);
                break;

            case R.id.dado2_img:
                n2 = randomNumero();
                dado2.setImageResource(caras[n2]);
                break;
        }
    }

    public void setCara(ImageView dado1, ImageView dado2 , int n1, int n2){
        if (n1 == 5 && n2 == 5){
            toast = Toast.makeText(this, "¡¡JACKPOT!!", Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.TOP, 0,0);
            toast.show();
        }
        dado1.setImageResource(caras[n1]);
        dado2.setImageResource(caras[n2]);
        dado1.setVisibility(View.VISIBLE);
        dado2.setVisibility(View.VISIBLE);
    }

    public int randomNumero(){
        return (int)(Math.random()*6);
    }
}


